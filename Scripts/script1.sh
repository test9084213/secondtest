#!/bin/bash

latest_commit_local=$(git rev-parse main)
echo $latest_commit_local
echo "-------------"
latest_commit_remote=$(git rev-parse origin/main)
echo $latest_commit_remote
echo "-------------"

if [ "$latest_commit_local" == "$latest_commit_remote" ]; then
  echo "Script exited, no differences found."
  exit 0
fi

new_tag=$(git rev-parse --short main)
echo $new_tag
echo "-------------"

echo "Enter your dockerhub username"
read docker_username
echo "Enter your dockerhub password"
read -s docker_password

if [[ ! $(echo "$docker_password" | docker login --username "$docker_username" --password-stdin) ]]; then
  exit 1
fi

#echo "$docker_password" | docker login --username "$docker_username" --password-stdin
docker build -t redonbasha/reat-todo-app:$new_tag react-todo-app/
docker push redonbasha/reat-todo-app:$new_tag

helm upgrade test3 ./react-todo-app-chart --set deployment.containers.imageTag=$new_tag
helm list -aA

